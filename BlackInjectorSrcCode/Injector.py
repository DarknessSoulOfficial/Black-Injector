import LoadLib_Injector as loadlibrary
import configparser
import os
def ReadConfigAndGetProcessName():
    config = configparser.ConfigParser()
    config.read_file(open(os.getcwd() + "\\Configs\\injector_conf.ini"))
    process_name = config.get('Injector', 'ProcessName')
    return process_name
def ReadConfAndGettingDllName():
    config = configparser.ConfigParser()
    config.read_file(open(os.getcwd() + "\\Configs\\injector_conf.ini"))
    dllname = config.get('Injector', 'DllName')
    return dllname
def InjectViaManualMap():
    return loadlibrary.LoadLib_LibInjector().AttachProcessAndManualMap(ReadConfigAndGetProcessName(), ReadConfAndGettingDllName())