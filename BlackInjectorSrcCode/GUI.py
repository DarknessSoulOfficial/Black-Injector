import dearpygui.dearpygui as dpg
import Injector as inject
import tkinter.messagebox as msg
import os
class GUI_DPG:
    def Init():
        dpg.create_context()
        with dpg.font_registry():
            consolafont = dpg.add_font("{}".format(os.getcwd() + "\\Assets\\consolafont.ttf"), size=15)
        with dpg.window(label="BlackInjector", tag="BLK_WINDOW"):
            dpg.add_text("This Is My First Injector for Anyone Game... Enjoy!!! :3", color=[145, 210, 55])
            with dpg.menu_bar():
                with dpg.menu(label="Inject As..."):
                    dpg.add_button(label="Manual Map", callback=inject.InjectViaManualMap)
                    dpg.add_button(label="LoadLibrary Injection", callback=lambda: msg.showwarning(title="BlackInjector", message="Soon!!!"))
        dpg.create_viewport(title='BlackInjector By DarknessSoul', width=540, height=725)
        dpg.bind_font(consolafont)
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.set_primary_window("BLK_WINDOW", True)
        dpg.start_dearpygui()
        dpg.destroy_context()