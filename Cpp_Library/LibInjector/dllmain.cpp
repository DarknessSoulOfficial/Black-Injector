// dllmain.cpp : Defines the entry point for the DLL application.
#include <Windows.h>
#include <BlackBone/Process/Process.h>
#include <iostream>
using namespace std;
using namespace blackbone;
std::set<std::wstring> nativeMods, modList;
#define PY_DLLEXPORT extern "C" __declspec(dllexport)
namespace LibInjector {
    NTSTATUS process_status = 0; //Getting STATUS Code via BlackBone :D
    blackbone::Process proc;
}
PY_DLLEXPORT void AttachProcessAndManualMap(const char* processname, const char* filename_dll) {
    nativeMods.clear();
    modList.clear();
    LibInjector::process_status = LibInjector::proc.Attach((const wchar_t*)processname);
    if (LibInjector::process_status) {
        MessageBoxA(0, "Attaching Is Failed!!!", "LibInjector Library", MB_ICONERROR | MB_OK);
        exit(4412);
    }
    else {
        MessageBoxA(0, "Successfully Attached!!!", "LibInjector Library", 0);
    }
    nativeMods.emplace(L"combase.dll");
    nativeMods.emplace(L"user32.dll");
    modList.emplace(L"windows.storage.dll");
    modList.emplace(L"shell32.dll");
    modList.emplace(L"shlwapi.dll");
    auto callback = [](CallbackType type, void* /*context*/, Process& /*process*/, const ModuleData& modInfo)
    {
            if (type == PreCallback)
            {
                if (nativeMods.count(modInfo.name))
                    return LoadData(MT_Native, Ldr_None);
            }
            else
            {
                if (modList.count(modInfo.name))
                    return LoadData(MT_Default, Ldr_ModList);
            }

            return LoadData(MT_Default, Ldr_None);
    }; //Calling Callbacks for Manual Map :D
    auto image = LibInjector::proc.mmap().MapImage((const wstring&)filename_dll, NoFlags, callback);
    if (!image) {
        MessageBoxA(0, "Failed To Attaching Current DLL to Current Process!!!", "LibInjector Library", 0);
        exit(1221);
    }
    else {
        MessageBoxA(0, "Success... Enjoy to using this Program!!! :3", "LibInjector Library", 0);
    }
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        MessageBoxA(0, "Attached Success!!!", "LibInjector Library", MB_ICONWARNING | MB_OK);
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

